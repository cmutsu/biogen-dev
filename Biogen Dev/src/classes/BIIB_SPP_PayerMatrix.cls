public class BIIB_SPP_PayerMatrix {
Public String strHealthCareAccount {get;set;}
Public Boolean isValidPatient {get;set;}
private work__c Owork;
String patientId = '';
String strPatientPrdName = '';
List<Affiliation_vod__c> lstAffiliated = new List<Affiliation_vod__c>();
private List<PreferredSPPPlan__c> lstPlan = new List<PreferredSPPPlan__c>();

    public BIIB_SPP_PayerMatrix(ApexPages.StandardController controller) { 
        Owork =(work__c)controller.getRecord();
        getDisplay();   
    }
    
    public void getDisplay()
    {
        patientId = Owork.BIIB_Account__c;
        if(patientId != NULL && patientId !=''){
            Account a = [Select Id, Name, BIID_Product__c from Account where Id = :patientId];
            strPatientPrdName = a.BIID_Product__c;  
        }
        
        if(Owork.BIIB_AccountRecordType__c == 'Patient')
        {
            isValidPatient = true;      
        }
        List<patient_plan__c> lstPatientPlan = new List<patient_plan__c>();
        lstAffiliated = [SELECT Id, BIIB_End_Date__c,To_Account_vod__c FROM Affiliation_vod__c WHERE From_Account_vod__c =:patientId and BIIB_End_Date__c= null and To_Account_vod__r.Recordtype.Name='SPP'];
        system.debug('**'+lstAffiliated);
        if(lstAffiliated.size() > 0)
            strHealthCareAccount = lstAffiliated[0].To_Account_vod__c;
        lstPatientPlan = [SELECT BIIB_Plan__c FROM patient_plan__c WHERE BIIB_Patient_Lookup__c = :patientId];
        
        Set<ID> stPlanId = new Set<Id>();
        
        for(patient_plan__c opp: lstPatientPlan)
        {
            stPlanId.add(opp.BIIB_plan__c);
        }
        system.debug('*'+stPlanId);
        lstPlan = [SELECT Id, name, Plan__c, spp1__c,spp1__r.Name,spp2__c,spp2__r.Name,ProductList__c FROM PreferredSPPPlan__c WHERE Plan__c IN : stPlanId and ProductList__r.Name = :strPatientPrdName order by spp1__r.Name, spp2__r.Name ];
        
        system.debug('*'+lstPlan);
        
    }
    public void Save()
    {
        if(strHealthCareAccount != Null)
        {
            if(lstAffiliated.size() > 0)
            {
                lstAffiliated[0].BIIB_End_Date__c = system.today();
                try{
                    update lstAffiliated[0];
                }catch(Exception e){
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
                    ApexPages.addMessage(myMsg);
                }
            }
       
            //create a new record with patient id and healthcare id and effective date as today.
       
            Affiliation_vod__c objAff=new Affiliation_vod__c(From_Account_vod__c =patientId,BIIB_Effective_Date__c=system.today(),To_Account_vod__c=strHealthCareAccount);
            try{
                insert objAff;
            }catch(Exception e){
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
                ApexPages.addMessage(myMsg);
            }
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,' Record Saved Successfully ');
            ApexPages.addMessage(myMsg);
            getDisplay();
        }
        else
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,' Please Select Any One SPP ');
            ApexPages.addMessage(myMsg);
        }
    }
    public List<SelectOption> getItems() {
        Set<Id> SPPId = new Set<Id>();
        List<SelectOption> options = new List<SelectOption>(); 
        for(PreferredSPPPlan__c objPlan : lstPlan){
            if(!SPPID.contains(objPlan.spp1__c)){
                options.add(new SelectOption(String.valueOf(objPlan.spp1__c),String.valueOf(objPlan.spp1__r.Name) + ' **'));
                SPPId.add(objPlan.spp1__c);
            }
        } 
        for(PreferredSPPPlan__c objPlan : lstPlan){
            if(!SPPId.contains(objPlan.spp2__c)){
               options.add(new SelectOption(String.valueOf(objPlan.spp2__c),String.valueOf(objPlan.spp2__r.Name) + ' **'));
               SPPId.add(objPlan.spp2__c);
            }
        } 
        for (Account a : [Select Id, Name from Account where Recordtype.Name ='SPP' and Id NOT in :SPPId]){
            options.add(new SelectOption(a.Id, a.Name));
            SPPId.add(a.Id);
        }        
        system.debug('options -- ' + options);
        return options;
    }
}