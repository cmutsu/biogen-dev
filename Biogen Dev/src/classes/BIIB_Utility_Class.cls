public with sharing class BIIB_Utility_Class {
    private static boolean run = true;
    
    public static boolean runOnce()
    {
        if(run)
        {
            run=false;
            return true;
        }
        else
        {
            return run;
        }
    }
    
    // Returns the record type ID based on Object and Record Type Label input parameters
    public static ID getRecordTypeId(String ObjectType, String RecordTypeLabel){   
        SObject OBJ;  
        Id rectypeId =null;
        // Describing Schema  
        Schema.SObjectType Res = Schema.getGlobalDescribe().get(ObjectType);  
        if (Res != null){  
            OBJ = Res.newSObject();  
            // Describing Object   
            Schema.DescribeSObjectResult DesRes = OBJ.getSObjectType().getDescribe();   
            if (DesRes != null){  
                Map<string, schema.recordtypeinfo> RecordTypeMap = DesRes.getRecordTypeInfosByName();  
                system.debug('--RecordTypeMap ' + RecordTypeMap );
                if (RecordTypeMap != null){  
                    Schema.RecordTypeInfo RecordTypeRes = RecordTypeMap.get(RecordTypeLabel);  
                    if (RecordTypeRes != null){  
                       rectypeId = RecordTypeRes.getRecordTypeId();  
                    }  
                }  
            }  
        }  
        system.debug('--rectypeId --' + rectypeId);
        return rectypeId;      
    }
    
    // Validates whether the Field API Name exists in the SObject
    public static boolean validatefieldAPIName(String ObjectType, String FieldAPIName){   
        // Describing Schema  
        Schema.SObjectType objSchema = Schema.getGlobalDescribe().get(ObjectType);
        Map<String, Schema.SObjectField> fieldMap = objSchema.getDescribe().fields.getMap();
        system.debug('--fieldMap --' + fieldMap);
        system.debug('--objSchema --' + objSchema);  
        if(fieldMap.containskey(FieldAPIName)){
            return true;
        }else{
            return false;           
        }
    }
    
    // Validates whether the Object API name entered is valid
    public static boolean validateObjectAPIName(String ObjectType){   
        // Describing Schema  
        Map<String, Schema.SObjectType> gdObjectAPINameMap = Schema.getGlobalDescribe(); 
        system.debug('--gdObjectAPIName --' + gdObjectAPINameMap);
        if(gdObjectAPINameMap.containskey(ObjectType)){
            return true;
        }else{
            return false;           
        }
    }
    
    // Validates the selected Operator and field combination is valid
    public static boolean validateFieldOperator(String strObjectAPIName,String strFieldAPIName,String strOperator)    
    {
          boolean result = false;
          // Describing Object 
          Schema.SObjectType Res = Schema.getGlobalDescribe().get(strObjectAPIName);  
          if (Res != null)
          {  
              // Describing Fields
              Map<String, Schema.SObjectField> fieldMap = Res.getDescribe().fields.getMap();
              if (fieldMap != null)
              {             
                  // Get field Data Type
                  Schema.DisplayType strFieldDataType=fieldMap.get(strFieldAPIName).getDescribe().getType();
                  String strFieldDataTypeConverted =  String.valueOf(fieldMap.get(strFieldAPIName).getDescribe().getType());
                  System.debug('-----------------FieldDataType--------'+strFieldDataType);
                  System.debug('-----------------strFieldDataType1--------'+strFieldDataTypeConverted);
                  for(BIIB_OperatorValidation__c objOperatorValidation: [SELECT Name,BIIB_Operator_Value__c,BIIB_Field_Data_Type__c FROM BIIB_OperatorValidation__c where BIIB_Field_Data_Type__c =:strFieldDataTypeConverted])
                  {
                    if(strOperator == objOperatorValidation.BIIB_Operator_Value__c)
                    {
                      System.debug('-----------------Operator Match--------');
                      result = true;
                      break;
                    }
                    
                    else
                    {
                      System.debug('-----------------Operator MisMatch--------');
                      result = false;
                    }
                    
                  }
                
               }
        }
        
        return result;

    }
    
    // Return the Map of Record Type Id and Record type Developer Name based on the Object passed as the input parameter
    public static Map<Id,String> caseRTDevName (String objectName){
        List<RecordType> rtList = new List<RecordType>();
        Map<Id,String> recordTypeMap = new Map<Id,String>();
        
        String strQuery = 'Select Id, DeveloperName from RecordType  Where SobjectType =\'' + objectName +'\'';
        rtList=database.query(strQuery);
        for(RecordType r : rtList){
            recordTypeMap.put(r.Id,r.DeveloperName);
        }
        return recordTypeMap;
    }
    
    //Check to ensure first and last characters are either a number or a closing/opening bracket
    public static boolean validateFirstLastChars(String strRuleLogic){
        strRuleLogic = strRuleLogic.replace('(','&').toUpperCase();
        strRuleLogic = strRuleLogic.replace(')','*').toUpperCase();
        return true;
        
    }
}