public with sharing class BIIB_Patient_Plan_Handler_Class {

    public static void OnBeforeInsert(List<Patient_Plan__c> newppList) {
        if(newppList != null && !newppList.isEmpty()) {
            primaryPlan(newppList);
        }
    }
    
    public static void OnBeforeUpdate(Map<Id,Patient_Plan__c> oldppMap, Map<Id,Patient_Plan__c> newppMap) {
        if(newppMap != null && !newppMap.isEmpty()) {
            primaryPlan(newppMap.values());
        }
    }
    
    // To allow only one primary Patient Plan for a Patient
    static void primaryPlan(List<Patient_Plan__c> triggerppList){
        
        Set<Id> patientIdSet = new Set<Id>();
        Set<Id> patientPlanIdSet = new Set<Id>();
        List<Patient_Plan__c> ppList = new List<Patient_Plan__c>();
        List<Patient_Plan__c> updppList = new List<Patient_Plan__c>();
        
        for (Patient_Plan__c pp: triggerppList){
            if(pp.BIIB_Primary__c){
                patientIdSet.add(pp.BIIB_Patient_Lookup__c);
                patientPlanIdSet.add(pp.Id);
            }       
            
        }
        ppList = [Select Id, Name, BIIB_Primary__c from Patient_Plan__c where BIIB_Patient_Lookup__c in :patientIdSet and BIIB_Primary__c = true and Id NOT in :patientPlanIdSet];
        for (Patient_Plan__c pp : ppList ){
            Patient_Plan__c p = new Patient_Plan__c();
            p = pp;
            p.BIIB_Primary__c = false;
            updppList.add(p);
        }
        try{
            update updppList;
        }catch(Exception e){
            for(Patient_Plan__c pp : updppList){
                pp.addError(e.getMessage());
            }
        }
    }

}