public class BIIB_EditMultipleWorkItem
{
    Public List<Work__c> lstWork {get;set;}
    private Case objCase = new Case();
    public BIIB_EditMultipleWorkItem(ApexPages.StandardController ctrl)
    {
        objCase  = (Case)ctrl.getRecord();
        if(objCase.Id != null)
        {
            lstWork  = [SELECT Name, BIIB_Assigned_to__c, BIIB_Status__c, BIIB_Preferred_Language__c FROM Work__c WHERE BIIB_Case__c=:objCase.Id];
        }
    }
    
    public pagereference Save()
    {
        try
        {
            update lstWork;
            PageReference pgRef = new PageReference('/'+objCase.Id);
            pgRef.setRedirect(true);
            return pgRef;
        }
        catch(Exception e)
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
            ApexPages.addMessage(myMsg);
        }
        return null;
    }
}