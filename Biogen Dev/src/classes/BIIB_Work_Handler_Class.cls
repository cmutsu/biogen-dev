public with sharing class BIIB_Work_Handler_Class 
{
    public static void OnBeforeInsert(List<Work__c> newWorkList) 
    {
        if(newWorkList!= null && !newWorkList.isEmpty()) 
        {
            preventCaseDuplicates(newWorkList);
        }
    }
      
    static void preventCaseDuplicates(List<Work__c> triggerWorkList) 
    {
        //Query custom setting and create a map with reordtype name as key and permission(Not Allowed?) as value 
        Map<String,Boolean> mapWorkTypeToPermission = new Map<String,Boolean>();
        Map<String,Map<String,Integer>> mapCaseIdToWorkRecordTypeToCount = new Map<String,Map<String,Integer>>();
        Map<String,String> mapWorkRecordTypeToDeveloperName = new Map<String,String>();
        for(BIIB_Work_Item_RecordType__c workRecordtypeList : BIIB_Work_Item_RecordType__c.getAll().values())
        {
            if(!workRecordtypeList.BIIB_Multiple_Allowed__c)
                mapWorkTypeToPermission.put(workRecordtypeList.Name,workRecordtypeList.BIIB_Multiple_Allowed__c);
        }
        system.debug('--------------'+mapWorkTypeToPermission);
        //loop thorugh the case list and create a set of Account ids
        Set<Id> setCaseId = new Set<Id>();
        for(Work__c objWork : triggerWorkList)
        {
            if(objWork.BIIB_Case__c != null) setCaseId.add(objWork.BIIB_Case__c);
        }
        
        system.debug('---------setCaseId--------'+setCaseId);
        
        //Querying all Existing case records related to Account and framing a map of Account Id to Map of Case Record Type and Count
        for(Work__c objWork : [    SELECT RecordType.DeveloperName, RecordType.Id, RecordTypeId, BIIB_Case__r.RecordTypeId, BIIB_Case__c FROM Work__c 
                                   WHERE BIIB_Case__c IN:setCaseId AND BIIB_Status__c!='Completed'])
        {
            mapWorkRecordTypeToDeveloperName .put(objWork.RecordType.Id,objWork.RecordType.DeveloperName);
            if(!mapCaseIdToWorkRecordTypeToCount.containskey(objWork.BIIB_Case__c))
            {
                Map<String,Integer> mapWorkRecorTypeToCount = new Map<String,Integer>();
                mapWorkRecorTypeToCount.put(objWork.RecordTypeId,1);
                mapCaseIdToWorkRecordTypeToCount.put(objWork.BIIB_Case__c,mapWorkRecorTypeToCount);
            }
            else if(mapCaseIdToWorkRecordTypeToCount.containskey(objWork.BIIB_Case__c) 
            && mapCaseIdToWorkRecordTypeToCount.get(objWork.BIIB_Case__c).containsKey(objWork.RecordTypeId))
            {
                Integer count = mapCaseIdToWorkRecordTypeToCount.get(objWork.BIIB_Case__c).get(objWork.RecordTypeId);
                count++;
                mapCaseIdToWorkRecordTypeToCount.get(objWork.BIIB_Case__c).put(objWork.RecordTypeId,count);
            }
            else if(mapCaseIdToWorkRecordTypeToCount.containskey(objWork.BIIB_Case__c) 
            && !mapCaseIdToWorkRecordTypeToCount.get(objWork.BIIB_Case__c).containsKey(objWork.RecordTypeId))
            {
                mapCaseIdToWorkRecordTypeToCount.get(objWork.BIIB_Case__c).put(objWork.RecordTypeId,1);
            } 
        }
        
        system.debug('------mapCaseIdToWorkRecordTypeToCount--------'+mapCaseIdToWorkRecordTypeToCount);
        
        for(Work__c objWork : triggerWorkList)
        {
            String strRecDeveloperName = mapWorkRecordTypeToDeveloperName .get(objWork.RecordTypeId);
            if(strRecDeveloperName != null && mapWorkTypeToPermission.containsKey(strRecDeveloperName))
            {
                system.debug('------objWork.RecordType.DeveloperName--------'+objWork.RecordType.DeveloperName);
                if(objWork.BIIB_Case__c != Null && mapCaseIdToWorkRecordTypeToCount.containskey(objWork.BIIB_Case__c)
                && mapCaseIdToWorkRecordTypeToCount.get(objWork.BIIB_Case__c).containsKey(objWork.RecordTypeId) 
                && mapCaseIdToWorkRecordTypeToCount.get(objWork.BIIB_Case__c).get(objWork.RecordTypeId) > 0)
                {
                    objWork.addError('There is already open Work with the same type on the same Service Request');
                }
                else if(objWork.BIIB_Case__c != Null)
                {
                    if(!mapCaseIdToWorkRecordTypeToCount.containskey(objWork.BIIB_Case__c))
                    {
                        Map<String,Integer> mapWorkRecorTypeToCount = new Map<String,Integer>();
                        mapWorkRecorTypeToCount.put(objWork.RecordTypeId,1);
                        mapCaseIdToWorkRecordTypeToCount.put(objWork.BIIB_Case__c,mapWorkRecorTypeToCount);
                    }
                    else if(mapCaseIdToWorkRecordTypeToCount.containskey(objWork.BIIB_Case__c) 
                    && mapCaseIdToWorkRecordTypeToCount.get(objWork.BIIB_Case__c).containsKey(objWork.RecordTypeId))
                    {
                        Integer count = mapCaseIdToWorkRecordTypeToCount.get(objWork.BIIB_Case__c).get(objWork.RecordTypeId);
                        count++;
                        mapCaseIdToWorkRecordTypeToCount.get(objWork.BIIB_Case__c).put(objWork.RecordTypeId,count);
                    }
                    else if(mapCaseIdToWorkRecordTypeToCount.containskey(objWork.BIIB_Case__c) 
                    && !mapCaseIdToWorkRecordTypeToCount.get(objWork.BIIB_Case__c).containsKey(objWork.RecordTypeId))
                    {
                        mapCaseIdToWorkRecordTypeToCount.get(objWork.BIIB_Case__c).put(objWork.RecordTypeId,1);
                    }
                }
            }
            else if(objWork.BIIB_Case__c != Null)
            {
                if(!mapCaseIdToWorkRecordTypeToCount.containskey(objWork.BIIB_Case__c))
                {
                    Map<String,Integer> mapWorkRecorTypeToCount = new Map<String,Integer>();
                    mapWorkRecorTypeToCount.put(objWork.RecordTypeId,1);
                    mapCaseIdToWorkRecordTypeToCount.put(objWork.BIIB_Case__c,mapWorkRecorTypeToCount);
                }
                else if(mapCaseIdToWorkRecordTypeToCount.containskey(objWork.BIIB_Case__c) 
                && mapCaseIdToWorkRecordTypeToCount.get(objWork.BIIB_Case__c).containsKey(objWork.RecordTypeId))
                {
                    Integer count = mapCaseIdToWorkRecordTypeToCount.get(objWork.BIIB_Case__c).get(objWork.RecordTypeId);
                    count++;
                    mapCaseIdToWorkRecordTypeToCount.get(objWork.BIIB_Case__c).put(objWork.RecordTypeId,count);
                }
                else if(mapCaseIdToWorkRecordTypeToCount.containskey(objWork.BIIB_Case__c) 
                && !mapCaseIdToWorkRecordTypeToCount.get(objWork.BIIB_Case__c).containsKey(objWork.RecordTypeId))
                {
                    mapCaseIdToWorkRecordTypeToCount.get(objWork.BIIB_Case__c).put(objWork.RecordTypeId,1);
                }
            }
        }           
    }
}