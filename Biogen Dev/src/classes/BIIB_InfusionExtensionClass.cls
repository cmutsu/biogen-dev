public class BIIB_InfusionExtensionClass {
    
    public List<Account>infusionList{get;set;}
    List<Account> lstLocalAccount;
    public String selectedValue {get;set;}
    public String selectedRadius {get;set;}
    public String zip {get;set;}
    public boolean isSortingCall {get;set;}
    public boolean infusionTableRendered {get;set;}
    public boolean bflag {get;set;}
    public boolean bdistanceflag {get;set;}
    String patientId = '';
    Public String strHealthCareAccount {get;set;}
    List<Affiliation_vod__c> lstAffiliated = new List<Affiliation_vod__c>();
    public String strtouchId {get;set;}
    public String strCenterName {get;set;}
    
    private Integer pageNumber;
    private Integer pageSize;
    private Integer totalPageNumber;
    public Integer getPageNumber()
    {
        return pageNumber;
    }
    
    public Integer getPageSize()
    {
        return pageSize;
    }
    
    public Boolean getPreviousButtonEnabled()
    {
        return !(pageNumber > 1);
    }
    
    public Boolean getNextButtonDisabled()
    {
        if (lstLocalAccount == null) return true;
        else
        return ((pageNumber * pageSize) >= lstLocalAccount.size());
    }
    
    public Integer getTotalPageNumber()
    {
        //if(totalPageNumber == 0 && lstLocalAccount !=null)
       //{
        totalPageNumber = lstLocalAccount.size() / pageSize;
        Integer mod = lstLocalAccount.size() - (totalPageNumber * pageSize);
        if (mod > 0)
            totalPageNumber++;
        //}
        return totalPageNumber;
    }
    
    public PageReference nextBtnClick()
     {
        BindData(pageNumber + 1);
        return null;
    }
    
    public PageReference previousBtnClick()
     {
        BindData(pageNumber - 1);
        return null;
    }
    
    //Sorting
    private String sortDirection = 'ASC';
    private String sortExp = 'Name';
    
    public String sortExpression
    {
         get
         {
            return sortExp;
         }
         set
         {
            system.debug('*********'+sortExpression+'***valiue='+value);
           //if the column is clicked on then switch between Ascending and Descending modes
           if (value == sortExp)
             sortDirection = (sortDirection == 'ASC')? 'DESC' : 'ASC';
           else
             sortDirection = 'ASC';
           sortExp = value;
           system.debug('*********'+sortExpression+'***valiue='+value);
         }
    }
        
    public String getSortDirection() 
    {
        //if not column is selected 
        system.debug('*******&&'+sortExpression);
        if (sortExpression == null || sortExpression == '')
          return 'ASC';
        else
         return sortDirection;
    }
 
    public void setSortDirection(String value)
    {  
       sortDirection = value;
    }
        
     public BIIB_InfusionExtensionClass(ApexPages.StandardController controller)
     {
        Case Owork =(Case)controller.getRecord();
        infusionTableRendered = false;
        patientId = Owork.AccountId;
        pageNumber = 0;
        totalPageNumber = 0;
        pageSize = Integer.valueOf(Label.BIIB_Page_Size_Infusion_Search);
        
        //getDisplay();
         system.debug('*'+ApexPages.currentPage().getParameters().get('Id'));
     }
        
    public void createAffiliation()
    {
        try
        {
            system.debug('&&&&&'+strHealthCareAccount);
            if(patientId != Null && strHealthCareAccount != Null)
            {
                system.debug('&&&&&&&&&&&&'+strHealthCareAccount);
                system.debug('^^^^^^^'+patientId);
                lstAffiliated = [SELECT Id, BIIB_End_Date__c,To_Account_vod__c FROM Affiliation_vod__c WHERE From_Account_vod__c =:patientId and BIIB_End_Date__c = null];
                system.debug('**'+lstAffiliated);
                if(lstAffiliated.size() > 0)
                {
                    lstAffiliated[0].BIIB_End_Date__c = system.today();
                    update lstAffiliated[0];
                }
           
                //create a new record with patient id and healthcare id and effective date as today.
                Affiliation_vod__c objAff=new Affiliation_vod__c(From_Account_vod__c =patientId,BIIB_Effective_Date__c=system.today(),To_Account_vod__c=strHealthCareAccount);
                insert objAff;
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,' Record Saved Successfully ');
                ApexPages.addMessage(myMsg);
                strHealthCareAccount=null;
            }
            else 
            {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,' Service Request is not linked to valid patient ');
                ApexPages.addMessage(myMsg);
            }
        }
        catch(Exception e)
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getdmlMessage(0));
            ApexPages.addMessage(myMsg);
        }
    }
              
    public List<SelectOption> getItems() 
    {
        List<SelectOption> options = new List<SelectOption>();
        List<BIIB_State__c> stateList=BIIB_State__c.getAll().values();
        stateList.sort();
        options.add(new SelectOption('None', 'None'));
        for(BIIB_State__c setting : stateList)
        {
            options.add(new SelectOption(setting.name, setting.name)); 
        } 
        return options;
    }
    
    public List<SelectOption> getDistance()
     {
        List<SelectOption>values = new List<SelectOption>();
        values.add(new SelectOption('Please Choose','Please Choose'));
        values.add(new SelectOption('5','5'));
        values.add(new SelectOption('10','10'));
        values.add(new SelectOption('15','15'));
        values.add(new SelectOption('20','20'));
        values.add(new SelectOption('25','25'));
        return values;
    }
    
    public void ShowResult()
    {
        bdistanceflag=false;
    
        String strQuery='select ID,Name,BIIB_Center__c,BillingStreet,BillingState, BillingPostalCode, BillingCountry,BillingCity,Phone,BIIB_Accepts_Referral__c,Distance_Between__c,BIIB_Site_Authorization_Status__c,BIIB_Insurance_Accepted__c,BIIB_Touch_Center_ID__c from Account where RecordType.Name = \''+String.escapeSingleQuotes('Infusion Sites')+'\' and ';
        
        Integer intState = (!selectedValue.equals('None')) ? 1 : 0;
        Integer intZipRadius = (!selectedRadius.equals('Please Choose') || (zip.length()>0)) ? 1 : 0;
        Integer intTouchId = (strTouchId.length()>0) ? 1 : 0;
        Integer intCenterName = (strCenterName.length()>0) ? 1 : 0;
        Integer intTotal= intState + intZipRadius + intTouchId + intCenterName;
               
        
        if(intTotal > 1 )
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,' Please search by only one search criteria at a time');
            ApexPages.addMessage(myMsg);
            return;
        }
        else if(intTotal == 0)
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,' Please search by at least one search criteria');
            ApexPages.addMessage(myMsg);
            return;
        } 
        else if(!selectedValue.equals('None'))
        {
            strQuery += 'BillingState like :selectedValue';
            bflag=true;
        }
        else if(strTouchId.length()>0)
        {
            strQuery += 'BIIB_Touch_Center_ID__c = :strTouchId';
            bflag=true;
        }
        else if(strCenterName.length()>0)
        {
            strQuery += 'Name like \'%' + strCenterName +'%\'';
            bflag=true;
        }
        else if(zip.length()<5 && selectedRadius.equals('Please Choose'))
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,' The Zip Code you entered is less than 5 numbers.Please Check the zip code.Please enter Radius');
            ApexPages.addMessage(myMsg);
            return;
        }
        else if(zip.length()<5)
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'The Zip Code you entered is less than 5 numbers.Please Check the zip code');
            ApexPages.addMessage(myMsg);
            return;
        }
        else
        {
            if(selectedRadius.equals('Please Choose'))
            {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,' Please enter Radius');
                ApexPages.addMessage(myMsg);
                return;
            }
            else
            {
                bdistanceflag = true;
                String strZipCode = getGeolocationFromZip(zip);
                
                system.debug('--------strZipCode-------'+strZipCode);
                if(strZipCode != null && strZipCode != '' && strZipCode.contains('-'))
                {
                    String strLat = strZipCode.split('LON')[0];
                    String strLon = strZipCode.split('LON')[1];
                    system.debug('--------strLat-------'+strLat);
                    system.debug('--------strLon-------'+strLon);
                    User objUser = new User(Id=userinfo.getUserId(), Location__Longitude__s = Double.valueOf(strLon), Location__Latitude__s = Double.valueOf(strLat));
                    update objUser;
                    strQuery += 'DISTANCE(Location__c, GEOLOCATION('+strLat+','+strLon+'), \''+String.escapeSingleQuotes('mi')+'\') < '+selectedRadius;
                }
            }
        }
         
        string sortFullExp = sortExpression  + ' ' + sortDirection;
        lstLocalAccount = new List<Account>();
        system.debug('--strQuery-----------'+strQuery);
        lstLocalAccount = Database.query(strQuery+' order by ' + sortFullExp);
        system.debug('-------------'+lstLocalAccount.size());
        system.debug('------isSortingCall-------'+isSortingCall);
        if (lstLocalAccount.isEmpty()){
            system.debug('--inside list size check--');
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'There are no Infusion Sites which match the search criteria');
            ApexPages.addMessage(myMsg);
        }
        
        if(!isSortingCall)
            BindData(1);
        else
            BindData(pageNumber);
    }
    
    public String getGeolocationFromZip(String strZipCode)
    {
        strZipCode = EncodingUtil.urlEncode(strZipCode, 'UTF-8');
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint('https://maps.googleapis.com/maps/api/geocode/json?address='+strZipCode+'&sensor=false&key='+System.Label.BIIB_Google_Api_Key);
        req.setMethod('GET');
        req.setTimeout(60000);
        double lat = null;
        double lon = null;
        
        try{
            // callout
            HttpResponse res = h.send(req);
            system.debug('--res--' + res.getBody());
 
            // parse coordinates from response
            JSONParser parser = JSON.createParser(res.getBody());
            system.debug('--parser--' + parser);
            lat = null;
            lon = null;
            while (parser.nextToken() != null) {
                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) &&
                    (parser.getText() == 'location')){
                       parser.nextToken(); // object start
                       while (parser.nextToken() != JSONToken.END_OBJECT){
                           String txt = parser.getText();
                           parser.nextToken();
                           if (txt == 'lat')
                               lat = parser.getDoubleValue();
                           else if (txt == 'lng')
                               lon = parser.getDoubleValue();
                       }
 
                }
            }
        }
        catch(Exception e) {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getmessage());
            ApexPages.addMessage(myMsg);
        }
        return lat+'LON'+lon;
    }
    
    public void BindData(Integer newPageIndex)
    {
            Transient Integer counter = 0;
            Transient Integer min = 0;
            Transient Integer max = 0;
            if (newPageIndex > pageNumber)
            {
                min = pageNumber * pageSize;
                max = newPageIndex * pageSize;
            }
            else
            {
                max = newPageIndex * pageSize;
                min = max - pageSize;

            }
            
            pageNumber = newPageIndex;
            infusionList = new List<Account>();
            for(Integer i=min; i < max ; i++)
            {
                if (lstLocalAccount.size() > i)
                    infusionList.add(lstLocalAccount[i]);
            }
            
            if(!infusionList.isEmpty() )
            {
                infusionTableRendered=true;
            }else{
                infusionTableRendered=false;
            }
    }
}