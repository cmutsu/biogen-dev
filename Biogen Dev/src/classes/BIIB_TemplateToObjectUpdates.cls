public with sharing class BIIB_TemplateToObjectUpdates
{
    
    Map<Id,Map<String,String>> mapUpdatedSRTemplates = new Map<Id,Map<String,String>>();
    Map<Id,Integer> mapTemplateDependency = new Map<Id,Integer>();
    Set<Id> setAccountId = new Set<Id>();
    Set<Double> setSequence = new Set<Double>();
    Map<String,Map<Double,Id>> mapParentCases = new Map<String,Map<Double,Id>>();
    
    public void BIIB_UpdateCases_FromSRTemplate(List<BIIB_SR_Template__c> lstnewRecords,Map<Id, BIIB_SR_Template__c> mapoldRecords)
    {

        List<BIIB_SRTemplate_To_SR_Mapping__c> lstSRTemplateToSR = [SELECT BIIB_SR_Field__c,BIIB_SR_Template_Field__c FROM BIIB_SRTemplate_To_SR_Mapping__c];
        for(BIIB_SR_Template__c objSRTemplateNew : lstnewRecords) 
        {
            SObject objSRTemplateNewObject = objSRTemplateNew;
            Schema.sObjectType objectDefSRTNew = Schema.getGlobalDescribe().get('BIIB_SR_Template__c').getDescribe().getSObjectType();
            SObject objSRTemplateNewData = objectDefSRTNew.newSObject();
            
            // Check in the map of oldrecord if it contains the RowId of the SRtemplates which has been updated.
            if(mapoldRecords.containsKey(objSRTemplateNew.Id))
            {
                for(BIIB_SRTemplate_To_SR_Mapping__c objCustomSetting :lstSRTemplateToSR)
                {
                    BIIB_SR_Template__c objOldSRTemplateData = mapoldRecords.get(objSRTemplateNew.Id);
                    SObject objOldSRT = objOldSRTemplateData;
                    Schema.sObjectType objectDefSRTOld = Schema.getGlobalDescribe().get('BIIB_SR_Template__c').getDescribe().getSObjectType();
                    SObject objSRTemplateOldData = objectDefSRTOld.newSObject();

                    // If the Dependency Field has been updated, then Create a Map of the SRTemplateId and the updated Dependency Value
                    if(objCustomSetting.BIIB_SR_Template_Field__c == 'BIIB_Dependency__c')
                    {
                        mapTemplateDependency.put(objSRTemplateNew.Id,Integer.valueOf(objSRTemplateNew.BIIB_Dependency__c));
                        
                        // Create a Set and Put the updated Dependency Values on each of the SR Templates
                        setSequence.add(Integer.valueOf(objSRTemplateNew.BIIB_Dependency__c));

                    }
                    
                    // This Loop is to check the Other set of SR Template Fields. If the Field Value is modified, then Create a Map<SRTemplateId, <Map<UpdatedField,UpdatedFieldValue>>
                    if(String.valueOf(objSRTemplateNewObject.get(objCustomSetting.BIIB_SR_Template_Field__c)) !=  String.valueOf(objOldSRT.get(objCustomSetting.BIIB_SR_Template_Field__c))) 
                    {
                        if(!mapUpdatedSRTemplates.containsKey(objSRTemplateNew.Id))
                        {
                            Map<String,String> mapUpdatedFields = new Map<String,String>();
                            mapUpdatedFields.put(objCustomSetting.BIIB_SR_Field__c,String.valueOf(objSRTemplateNewObject.get(objCustomSetting.BIIB_SR_Template_Field__c)));
                            mapUpdatedSRTemplates.put(objSRTemplateNew.Id,mapUpdatedFields);
                            
                        }
                        else
                        {
                            mapUpdatedSRTemplates.get(objSRTemplateNew.Id).put(objCustomSetting.BIIB_SR_Field__c,String.valueOf(objSRTemplateNewObject.get(objCustomSetting.BIIB_SR_Template_Field__c)));
                        }
                    }
                }
            }
        }

        
        // Update Fields On Case
        // Query All Cases where SRTemplateId exists in the mapUpdatedSRTemplates.i.e Query all cases for the SR Templates which have been updated and loop through it
        for(String Key :mapUpdatedSRTemplates.keySet())
        {

            List<Case> lstCases = new List<Case>([SELECT Id from Case where BIIB_SR_Template__c =: Key]);
            String strObjectName = 'Case';
            
            Schema.SObjectType Res = Schema.getGlobalDescribe().get(strObjectName);
            Map<String, Schema.SObjectField> fieldMap = Res.getDescribe().fields.getMap();
            
            for(Case objCase :lstCases)
            {
                //Inside For Loop
                SObject objCaseGenericObject = objCase;
                Map<String,String> mapFieldNameValue= new Map<String,String>();
                mapFieldNameValue = mapUpdatedSRTemplates.get(Key);

				// Create Case Object and Update field values for whatever corresponding fields that have been modified on the SR Template.
				//Here, check for all the Data Types is done as type casting will be needed based on field data type
                for(String KeyName :mapFieldNameValue.keySet())
                {
                    
                    Schema.DisplayType strFieldDataType=fieldMap.get(KeyName).getDescribe().getType();
                    String strFieldDataTypeConverted =  String.valueOf(fieldMap.get(KeyName).getDescribe().getType());
                    
                    if(strFieldDataTypeConverted == 'BOOLEAN')
                    {
                        objCaseGenericObject.put(KeyName,Boolean.valueOf(mapFieldNameValue.get(KeyName)));
                    }
                    
                    else if(strFieldDataTypeConverted == 'STRING')
                    {
                        objCaseGenericObject.put(KeyName,mapFieldNameValue.get(KeyName));
                    }
                    
                    else if(strFieldDataTypeConverted == 'INTEGER')
                    {
                        objCaseGenericObject.put(KeyName,Integer.valueOf(mapFieldNameValue.get(KeyName)));
                    }
                    else if(strFieldDataTypeConverted == 'DOUBLE')
                    {
                        objCaseGenericObject.put(KeyName,Double.valueOf(mapFieldNameValue.get(KeyName)));
                    }
                    else
                    {
                        objCaseGenericObject.put(KeyName,mapFieldNameValue.get(KeyName));
                    }
                }
                Case objFinalCase = (Case)objCaseGenericObject;
                update objFinalCase;
            }
        }
            
        // Update Parent Case Information Based on SR Template Updates
        
            // This is the List Of Cases for which the "Dependency" field has been updated on its related SR Template
            List<Case> lstCasesDependency = new List<Case>([SELECT Id,AccountId from Case where BIIB_SR_Template__c IN :mapTemplateDependency.keySet()]);
            
            
            //Based on the above list, we need to filter out the unique set of AccountId's for which their related SR's need to be updated for dependency functionality
            for(Case objCaseDetails : lstCasesDependency)
            {
                setAccountId.add(objCaseDetails.AccountId);
            }

            //  Query SR's where Seqeuence is in setSequence and AccountId in setAccountId. This will basically create a Map<AccountId, Map<ParentCaseSequenceNumber,ParentCaseId>>
            for (Case objParentCases: [SELECT Id,BIIB_Sequence_Number__c,AccountId,BIIB_Dependency__c FROM Case WHERE BIIB_Sequence_Number__c IN :setSequence AND AccountId IN :setAccountId AND Id NOT IN :lstCasesDependency])
            {
                //Map of <AccountId, <Map<SequenceNum,CaseId>>
                Map<Double,Id> mapSequenceCaseId = new Map<Double,Id>();
                mapSequenceCaseId.put(Double.valueOf(objParentCases.BIIB_Sequence_Number__c),objParentCases.Id);
                
                if(!mapParentCases.containsKey(objParentCases.AccountId))
                {
                    mapParentCases.put(objParentCases.AccountId,mapSequenceCaseId);
                }
                
                else
                {
                    mapParentCases.get(objParentCases.AccountId).put(objParentCases.BIIB_Sequence_Number__c,objParentCases.Id);
                }
            }

            // Query Cases where Account is in the Set
            for(List<Case> lstAccountCases: [SELECT Id,BIIB_Dependency__c,BIIB_Sequence_Number__c,AccountId FROM Case where AccountId IN :setAccountId])
            {
                for(Case objCaseUpdates :lstAccountCases)
                {
                    // Fetch the Value Of Dependency On the Case
                    Double intDependency = objCaseUpdates.BIIB_Dependency__c;
                    if(mapParentCases.containsKey(objCaseUpdates.AccountId))
                    {
                        //Fetch Parent CaseId and update on Child Case
                        Map<Double,Id> mapParentSequenceCaseId = new Map<Double,Id>();
                        mapParentSequenceCaseId = mapParentCases.get(objCaseUpdates.AccountId);
                        if(mapParentSequenceCaseId.containsKey(intDependency))
                        {
                            String strParentCaseId = mapParentSequenceCaseId.get(intDependency);
                            objCaseUpdates.ParentId = strParentCaseId;
                            update objCaseUpdates;
                            
                        }
                    }
                }
            }
    }
}