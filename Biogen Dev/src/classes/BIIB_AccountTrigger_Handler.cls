/*****************************************************************************

Class Name	: BIIB_AccountTrigger_Handler
Created By	: Charles Mutsu
Description	: Handle the Account Trigger Functions

*****************************************************************************/
public class BIIB_AccountTrigger_Handler {
	
	//Method Name	: updateHcpDoNotCallOnPatient
	//Created By	: Charles Mutsu
	//Parameters	: List of Account
	//Function		: Updates Patient Account's HCP Do not call field as True when HCP Account's Do not call field is True
	public static void updateHcpDoNotCallOnPatient(List<Account> lstAcc){
		
		Set<ID> setHcpAccIds = new Set<ID>();	//Set to collect HCP account ids
	    Set<ID> setPatientAccIds = new Set<ID>();	//Set to collect Patient account ids
	    List<Account> lstPatientAcc = new List<Account>();	//Collect the List of Patient Account to update
	    
	    //Collect HCP Account IDs from List of Account sent from Trigger
	    for(Account acc : lstAcc){
	    	if(acc.BIIB_Account_Profile_Type__c == 'HCP' && acc.PersonDoNotCall){
	    		setHcpAccIds.add(acc.Id);
	    	}
	    }
	    
	    //Collect Patient Account IDs from List of Affiliations between Patient and HCP
	    if(setHcpAccIds.size()>0){
		    for(BIIB_Affiliation__c aff:[Select BIIB_Affiliation_Type__c, BIIB_From_Account__c, BIIB_To_Account__c from BIIB_Affiliation__c where BIIB_From_Account__c IN:setHcpAccIds AND BIIB_Affiliation_Type__c = 'HCP - Patient']){
		    	if(aff.BIIB_To_Account__c != NULL){
		    		setPatientAccIds.add(aff.BIIB_To_Account__c);
		    	}
		    }
	    }
	    
	    //Collect the List of Patient Accounts to be updated
	    if(setPatientAccIds.size()>0){
	    	for(Account acc: [Select BIIB_Account_Profile_Type__c, BIIB_HCP_Do_Not_Call__c from Account where Id IN:setPatientAccIds AND BIIB_Account_Profile_Type__c = 'Patient']){
	    		acc.BIIB_HCP_Do_Not_Call__c = True;
	    		lstPatientAcc.add(acc);	
	    	}
	    }
	    
	    //Update Patient Accounts with HCP Do Not Call field
	    if(lstPatientAcc.size()>0){
	    	update lstPatientAcc;
	    }
	}
}