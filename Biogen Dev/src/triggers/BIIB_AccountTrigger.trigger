trigger BIIB_AccountTrigger on Account(after insert, after update) 
{
    for(Account a : trigger.new) 
    {
        if(a.BIIB_Location__Latitude__s == null && BIIB_Utility_Class.runOnce())
            BIIB_LocationCallouts.getLocation(a.Id);
    }
}