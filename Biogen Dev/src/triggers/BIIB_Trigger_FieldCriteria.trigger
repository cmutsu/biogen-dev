trigger BIIB_Trigger_FieldCriteria on BIIB_Field_Criteria__c(before insert, before update)
{
	if (trigger.isBefore){
        if(trigger.isInsert || trigger.isUpdate){
            List<BIIB_Field_Criteria__c> fieldCriteriaList = new List<BIIB_Field_Criteria__c>();
            for (BIIB_Field_Criteria__c  fc: Trigger.New){
                fieldCriteriaList.add(fc);
            }
            for(BIIB_Field_Criteria__c fcItem: fieldCriteriaList) {
            	boolean bObjValid = BIIB_Utility_Class.validateObjectAPIName(fcItem.BIIB_Object_API_Name__c);
            	if(bObjValid)
            	{
	                boolean bOperatorvalid = BIIB_Utility_Class.validateFieldOperator(fcItem.BIIB_Object_API_Name__c, fcItem.BIIB_Field_API_Name__c, fcItem.BIIB_Operator__c);
	                if(!bOperatorvalid) {
	                	fcItem.addError('Operator ' + fcItem.BIIB_Operator__c + ' is not valid for selected field');
	                }
            	} else {
            		fcItem.addError('Object API name ' + fcItem.BIIB_Object_API_Name__c + ' is not valid');
            	}
            }
        }
    }
}