trigger BIIB_Affiliation_Trigger on BIIB_Affiliation__c(Before Insert, After Insert, Before Update, After update) 
{
    if(trigger.isBefore && trigger.isInsert)
    {
        BIIB_Affiliation_Handler.onBeforeInsert(trigger.new);
    }
    else if(trigger.isAfter && trigger.isInsert)
    {
        BIIB_Affiliation_Handler.onAfterInsert(trigger.newMap);
    }
    else if(trigger.isBefore && trigger.isUpdate)
    {
        BIIB_Affiliation_Handler.onBeforeUpdate(trigger.newMap);
    }
    else if(trigger.isAfter && trigger.isupdate)
    {
        BIIB_Affiliation_Handler.onAfterUpdate(trigger.newMap, trigger.oldMap);
    }
}