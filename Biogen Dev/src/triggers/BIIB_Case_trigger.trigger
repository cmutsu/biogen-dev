/*  Name: BIIB_Case_trigger
    Author: USI Offshore 
    Created Date:8-Apr-14
    Reason: 
       1. To allow multiple Open Cases for recordtypes which are specified in the custom setting BIIB_Case_RecordTypes__c where Multiple is true
*/
trigger BIIB_Case_trigger on Case (before insert) {
   
    //Before Insert 
    if(Trigger.isInsert && Trigger.isBefore){
        system.debug('new map' + Trigger.new);
        BIIB_Case_Handler_Class.OnBeforeInsert(Trigger.new);
    }
}