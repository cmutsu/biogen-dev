/*  Name: BIIB_Patient_Plan_trigger
	Author: USI Offshore 
	Created Date:8-Apr-14
    Reason: 
	   1. To allow only one primary Patient Plan for a Patient
*/
trigger BIIB_Patient_Plan_trigger on Patient_Plan__c (before Insert, before update) {
	
	/* Before Insert */
    if(Trigger.isInsert && Trigger.isBefore){
    	system.debug('new map' + Trigger.new);
        BIIB_Patient_Plan_Handler_Class.OnBeforeInsert(Trigger.new);
    }
    
    /* Before Update */
    else if(Trigger.isUpdate && Trigger.isBefore){
        BIIB_Patient_Plan_Handler_Class.OnBeforeUpdate (Trigger.OldMap , Trigger.newMap);
    }
}