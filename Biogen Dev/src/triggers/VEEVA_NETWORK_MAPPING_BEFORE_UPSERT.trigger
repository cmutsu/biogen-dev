trigger VEEVA_NETWORK_MAPPING_BEFORE_UPSERT on Network_Mapping_vod__c (before insert, before update) {
    for(Network_Mapping_vod__c mapping : Trigger.new) {
        if (mapping.Active_vod__c) {
            mapping.Unique_Key_vod__c = mapping.Country_vod__c;
        } else {
            mapping.Unique_Key_vod__c = null;
        }
    }
}