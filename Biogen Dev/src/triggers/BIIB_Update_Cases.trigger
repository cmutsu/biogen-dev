trigger BIIB_Update_Cases on BIIB_SR_Template__c (after update) 
{
    // Invoke Handler Class and pass Trigger.new and Trigger.oldMap to the class. The class will handle the further functionality
    
    BIIB_TemplateToObjectUpdates clsTemplateToObject = new BIIB_TemplateToObjectUpdates();
    clsTemplateToObject.BIIB_UpdateCases_FromSRTemplate(Trigger.new,Trigger.oldMap);
}